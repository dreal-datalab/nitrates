# La donnée

<img style="float:right;margin:5px 20px 5px 0px" src="photo_riviere_labo_hydro.jpg" width="40%">

Les données sont issues :

* pour les __eaux de surface__ (appelées __ESU__) : par les données de la base Naiades géré par l’Agence de l’eau Loire-Bretagne (AELB) et des données de l’Agence régionale de santé (ARS) des Pays de la Loire ;
    
* pour les __eaux souterraines__ (appelées __ESO__) : par les données de la base ADES gérée par le BRGM, complétées par des données de l’ARS Pays de la Loire.

Les données ADES et Naiades sont disponibles en open data sur les sites correspondants : 

- https://ades.eaufrance.fr/
- http://www.naiades.eaufrance.fr/

Ces données seront mises à jour régulièrement et l'application enrichie au fur et à mesure.


Les données alimentant la v1 correspondent à un export sur la période 2007-2018 d'ADES (à noter que l'année 2018 est incomplète sur les 6 derniers mois), les données ARS complémentaires non versées sur ADES et un export Naiades réalisés en juillet 2019.


_crédits photo : Laboratoire d'hydrobiologie de la DREAL des Pays de la Loire_     
    
