

library(shiny)
library(shinycssloaders)
library(shinydashboard)
library(shinythemes)
library(shinyBS)
library(bsplus)
library(sf)
library(leaflet)
library(leaflet.extras)
library(stringr)
library(dplyr)
library(magrittr)
library(forcats)
library(tidyr)
library(ggplot2)
library(hrbrthemes)
library(lubridate)
library(highcharter)
library(purrr)
library(ggridges)
library(knitr)
library(kableExtra)
library(formattable)
library(viridis)
library(scales)
library(shinydreal)

load("app.RData")
load("referentiels_carto.RData")
load("referentiels.RData")
options(scipen = 10000)