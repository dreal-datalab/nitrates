# Les seuils associés

<img style="float:right;margin:5px 20px 5px 0px" src="classes.png">

Pour le suivi des nitrates dans l’eau plusieurs seuils sont utilisés et correspondent à des objectifs différents. Les P90 sont ainsi représentés par classe de couleur intégrant les bornes 18 mg/L, 40mg/L et 50mg/L notamment. Ces bornes sont légèrement différentes du classement « seq’eau ».


__Limites « eau potable »__


* __limite de qualité dans l’eau brute__ destinée à la consommation humaine (arrêté du 11 janvier 2007 modifié, en application de la directive 98/83/CE) : ESO : 100 mg/L et ESU : 50 mg/L ;


* __limite de qualité dans l’eau traitée__ destinée à la consommation humaine : ESO et ESU : 50 mg/L.

    
__Limite au sens de la DCE__ : seuil de 50mg/L pour les ESU, la masse d’eau risque de ne pas atteindre le bon état écologique.


__Seuils utilisés pour la définition des zones vulnérables__ (notamment) :


* ESU : 18mg/L


* ESO : 50 mg/L


__Seuils utilisés pour la définition des ZAR__ (zones d’actions renforcées) :


* zones de captages AEP pour lesquelles le maximum du P90 des 2 dernières années est supérieur à 50mg/L ;


* ou s’il se situe entre 40 et 50mg/L et que la tendance n’est pas à la baisse.


